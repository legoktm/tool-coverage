#!/usr/bin/env python
"""
grab_from_jenkins.py - gets clover.xml files from jenkins to backfill
Copyright (C) 2017 Kunal Mehta <legoktm@member.fsf.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import os
import requests
import xml.etree.cElementTree as etree

JOB = "https://integration.wikimedia.org/ci/job/mediawiki-core-code-coverage/"

s = requests.Session()
path = os.path.expanduser("~/mediawiki")


def api_request(url):
    print(url)
    r = s.get(url + "api/json")
    r.raise_for_status()
    return r.json()


def strip_clover(fname):
    tree = etree.parse(fname)
    root = tree.getroot()
    project = root.getchildren()[0]
    assert project.tag == "project"
    for child in project.getchildren():
        # All we want to keep is the final <metrics> tag
        if child.tag != "metrics":
            project.remove(child)
    tree.write(fname)


def get_build(url):
    info = api_request(url)
    if info["result"] != "SUCCESS":
        return
    # jenkins timstamp is epoch BUT the last three digits
    # are microseconds I guess? Ignore them
    dt = datetime.datetime.utcfromtimestamp(int(str(info["timestamp"])[:-3]))
    fname = os.path.join(path, "clover-%s.xml" % str(dt).split(" ")[0])
    if os.path.exists(fname):
        return
    artifact = JOB + str(info["number"]) + "/artifact/log/clover.xml"
    print("Downloading clover.xml for %s" % info["number"])
    r = s.get(artifact, stream=True)
    with open(fname, "wb") as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

    print("Saved to %s" % fname)
    strip_clover(fname)
    print("Stripped extra info")


def main():
    info = api_request(JOB)
    for build in info["builds"]:
        get_build(build["url"])


if __name__ == "__main__":
    main()
