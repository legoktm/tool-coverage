#!/usr/bin/env python
"""
fetch.py - downloads MediaWiki core coverage results
Copyright (C) 2017 Kunal Mehta <legoktm@member.fsf.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import os
import re
import requests
import toolforge

toolforge.set_user_agent("coverage")
CLOVER = "https://doc.wikimedia.org/cover/mediawiki-core/clover.xml"


def download_clover(url, directory):
    r = requests.get(url)
    modified = r.headers["last-modified"]
    dt = datetime.datetime.strptime(modified, "%a, %d %b %Y %H:%M:%S GMT")
    fname = os.path.expanduser(
        directory + "/clover-%s.xml" % str(dt).split(" ")[0]
    )
    if os.path.isfile(fname):
        print("Already downloaded %s" % fname)
        return

    with open(fname, "w") as f:
        f.write(r.text)
    print("Saved to %s" % fname)


def main():
    download_clover(CLOVER, "~/mediawiki")
    r = requests.get("https://doc.wikimedia.org/cover-extensions/")
    extensions = re.findall('<a class="cover-item" href="./(.*?)/">', r.text)
    for extension in extensions:
        directory = os.path.expanduser("~/extensions/" + extension)
        if not os.path.isdir(directory):
            os.mkdir(directory)
        download_clover(
            "https://doc.wikimedia.org/cover-extensions/%s/clover.xml"
            % extension,
            directory,
        )


if __name__ == "__main__":
    main()
