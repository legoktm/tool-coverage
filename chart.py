#!/usr/bin/env python
"""
chart.py - charts MediaWiki core coverage results
Copyright (C) 2017 Kunal Mehta <legoktm@member.fsf.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
import matplotlib

matplotlib.use("svg")  # noqa
import matplotlib.pyplot as plot
import os
import xml.etree.cElementTree as etree


def find_files(path: str):
    return sorted(
        os.path.join(path, fname)
        for fname in os.listdir(path)
        if fname.endswith(".xml")
    )


def process_file(fname):
    date = datetime.datetime.strptime(
        os.path.basename(fname), "clover-%Y-%m-%d.xml"
    )
    print(date)
    tree = etree.parse(fname)
    root = tree.getroot()
    project = root.getchildren()[0]
    assert project.tag == "project"
    metrics = None
    for child in project.getchildren():
        if child.tag == "metrics":
            metrics = child
            break
    if metrics is None:
        raise ValueError("Unable to find <metrics> in %s!" % fname)

    stuff = ["methods", "conditionals", "statements", "elements"]
    total = sum(int(metrics.attrib[thing]) for thing in stuff)
    div = sum(int(metrics.attrib["covered%s" % thing]) for thing in stuff)
    covered = round(div / total * 100, 2)
    return [date, covered]


def make_chart(directory, name, save_fname):
    x = []
    y = []
    path = os.path.expanduser(directory)
    for fname in find_files(path):
        date, covered = process_file(fname)
        x.append(date)
        y.append(covered)

    plot.figure(figsize=(16, 8))
    plot.plot(x, y)
    plot.xlabel("Date")
    plot.ylabel("Percentage covered")
    plot.title(name + " code coverage")
    chart = os.path.expanduser(os.path.join("~/charts", save_fname + ".png"))
    plot.savefig(chart)
    plot.close()
    print("Created %s" % chart)


def main():
    make_chart("~/mediawiki", "MediaWiki core", "mediawiki")
    extension_dir = os.path.expanduser("~/extensions")
    extensions = sorted(os.listdir(extension_dir))
    for extension in extensions:
        make_chart(os.path.join(extension_dir, extension), extension, extension)


if __name__ == "__main__":
    main()
